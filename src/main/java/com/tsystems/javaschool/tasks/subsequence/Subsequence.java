package com.tsystems.javaschool.tasks.subsequence;


import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


public class Subsequence {


    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")

    public boolean find(List x, List y) {


        if ( x == null || y == null) throw new IllegalArgumentException(); // input check

        if (x.isEmpty()) return true;

        if (y.size() < x.size()) return false;

        Queue xQ = new LinkedList(x);  // using Queue for representation of List

        Queue yQ = new LinkedList(y);


        Object xObj = xQ.remove();  // upcasting to work with any items

        Object yObj;


        while (!yQ.isEmpty()){

            yObj = yQ.remove();

            if (yObj.equals(xObj)){

                if (yQ.isEmpty()||xQ.isEmpty()) return true;

                xObj = xQ.remove();
            }
        }

        return false;

    }
}
