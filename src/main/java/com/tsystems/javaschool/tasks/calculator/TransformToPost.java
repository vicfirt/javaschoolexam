package com.tsystems.javaschool.tasks.calculator;


import java.util.Stack;

class TransformToPost {  //This class converts infix notation to postfix

    private Stack<Character> signStack;
    private String input;
    private StringBuilder output = new StringBuilder();



    public TransformToPost(String in) // constructor
    {
        input = in;
        int stackSize = input.length();
        signStack = new Stack<>();
    }



    public String transform() { // // The method that performs the transformation

        for (int i = 0; i < input.length(); i++) {

            char ch = input.charAt(i);

            if (ch == ' ') continue;

            switch (ch) {

                case '+':    // + or -
                case '-':

                    output.append(' ');
                    gotOperator(ch, 1);  // getting operator
                    break;   // 1st priority

                case '*':
                case '/':

                    output.append(' ');
                    gotOperator(ch, 2);
                    break;  // 2nd priority

                case '(':

                    signStack.push(ch);  // add to stack
                    break;

                case ')':

                    int t = gotParen(ch);
                    if (t == 0) return "";

                    break;

                default:    // operand remains

                    output.append(ch);

                    break;

            }
        }


        while (!signStack.isEmpty()) // pop remaining operators
        {
            output.append(' ');
            output.append(signStack.pop()); // write to output
        }

        return output.toString();    // return postfix
    }

    public void gotOperator(char opThis, int prior1) {  //reading a statement from an input string

        while (!signStack.isEmpty()) {

            char opTop = signStack.pop();

            if (opTop == '(') {

                signStack.push(opTop);
                break;
            } else {

                int prior2;  // new operator priority

                if (opTop == '+' || opTop == '-') {  // evaluating priority

                    prior2 = 1;
                } else {

                    prior2 = 2;
                }

                if (prior2 < prior1) {

                    signStack.push(opTop);   //saving new operator
                    break;
                } else {

                    output.append(opTop).append(' '); // pushing a new operator
                }
            }
        }

        signStack.push(opThis);
    }


    public int gotParen(char ch) {

        if (signStack.size() < 2) return 0;  //matches that input expression incorrect

        while (!signStack.isEmpty()) {

            char x = signStack.pop();

            if (x == '(') {

                break;
            } else {

                output.append(' ').append(x); // output to postfix string

            }
        }
        return 1;
    }
}

