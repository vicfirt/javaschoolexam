package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    Stack<Double> stack;  // stack for values

    private int positionIndex;

    private String postfix = "";

    public Calculator(){

        stack = new Stack<>();
    }


    public String evaluate(String statement) {  //calculating method


        try {                                           // checking for null input

            TransformToPost transformator = new TransformToPost(statement);

            postfix = transformator.transform();  // trying to transform to postfix

        }catch (NullPointerException e) {return null;}

        double val;
        double temp = 0;
        double num1, num2;


        if(postfix.isEmpty()) return null;


        String[] tmp = postfix.split(" ");
        for(int j=0; j<tmp.length; j++){

            if(tmp[j].isEmpty()) return null;


            if(!tmp[j].equals("+") && !tmp[j].equals("-") &&
                    !tmp[j].equals("*") && !tmp[j].equals("/")){

                try{                            //if its not a number
                    val = Double.valueOf(tmp[j]);

                }catch(NumberFormatException ex){

                    return null;
                }
                stack.push(val);
            }
            else{   // if its an operator

                num2 = stack.pop();
                num1 = stack.pop();

                if(tmp[j].equals("+")){
                    temp = num1 + num2;
                }
                if(tmp[j].equals("-")){
                    temp = num1 - num2;
                }
                if(tmp[j].equals("*")){
                    temp = num1 * num2;
                }
                if(tmp[j].equals("/")){

                    if(num2 == 0)    //divide by zero protection
                        return null;
                    temp = num1 / num2;
                }
                stack.push(temp);
            }
        }


        Double result = stack.pop();

            //output rounding block

         if (result == Math.floor(result)) {
            return String.format("%.0f", result);
        }
         if (Double.toString(result).substring(Double.toString(result).indexOf('.')).length() > 5) {
            return String.format("%.4f", result);
        }
         else {
            return Double.toString(result);
        }

    }

}
