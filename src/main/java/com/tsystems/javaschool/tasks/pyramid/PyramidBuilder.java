package com.tsystems.javaschool.tasks.pyramid;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class PyramidBuilder {


    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {



        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException(); // null input protection


        int inputSize = inputNumbers.size();

        int rowNumber = getRowNumber(inputSize);

        int columnNumber = rowNumber *2 -1;

        if (rowNumber == -1) throw new CannotBuildPyramidException(); //if its not a triangular number

        int[][] pyramid = new int[rowNumber][columnNumber];

        Collections.sort(inputNumbers);

        int mid = columnNumber/2;

        for (int i = 0, inputIndexes = 0; i<rowNumber;i++){ //initializing pyramid

            for (int j = 0; j < i+1; j++, inputIndexes++){

                pyramid[i][mid - i + j * 2] = inputNumbers.get(inputIndexes);
            }
        }



        return pyramid;
    }

    private static int getRowNumber(int number) { // checking if a number is triangular

        double res = (Math.sqrt(number*8+1)-1)/2;

        if (res == Math.ceil(res)) return (int) res;

        return -1;


    }




    }
